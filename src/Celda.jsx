import React from "react"; 
import styled from 'styled-components';

const Celda = styled.div`
    width:198px;
    height: 150px;
    text-align: center;
    font-size: 100px;
    border:1px solid black;
    cursor: pointer;
`;

export default (props) => {
    //console.log(props.estado);

    let marca = '';

    switch (props.estado) {
        case 1:
            marca = 'X';
            break;  
            
        case 2:
            marca = 'O';
            break;
    
        default:
            break;
    }

    return(
        <Celda onClick={(props.estado == 0) ? props.clickCelda : null}>{marca}</Celda>
    );
}