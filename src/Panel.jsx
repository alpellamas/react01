import React from "react"; 
import styled from 'styled-components'

const Panel = styled.div`
    width: 600px;
    text-align: center;
    border: solid 3px grey;
    background-color: black;
    color: white;
    padding: 10px;
`;

export default (props) => {


    return(
        <Panel><h2>{props.mensaje}</h2></Panel>
    );
}