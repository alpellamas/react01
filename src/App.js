import React, { useEffect, useState } from "react"; // hook
import './estilos.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import {DivCentrado} from './Componentes';
import Tablero from './Tablero';




export default () => {

  let [mapa, setMapa] = useState([0,0,0,0,0,0,0,0,0]); 
  let [turno, setTurno] = useState(1); 
  let [gameover, setGameOver] = useState(false); 
  let [ganador, setGanador] = useState(0); 
  
  const WINNERS = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [0,4,8],
    [2,4,6]
  ];

  function clickCelda (num){
    //console.log("La celda clickada es "+ num);
    

      let auxiliar = [...mapa];
      
      if(turno == 1){
        auxiliar[num] = turno;

        setMapa(auxiliar);
        setTurno(turno = 2);

        //console.log(mapa);
        //console.log(turno);
      }else{
        auxiliar[num] = turno;

        setMapa(auxiliar);
        setTurno(turno = 1);

        //console.log(mapa);
        //console.log(turno);
      }
  }

  let mensaje = '';
  if(ganador != 0){
    console.log(ganador);
    mensaje = "Ganador jugador " + ganador;
  }else{
    if(gameover){
      mensaje = "GAME OVER";
    }else{
      mensaje = "Jugador " + turno;
    }
  }

  useEffect(() => {
    //console.log("useEffect");
    const zeros = mapa.filter(el => el===0).length;
    if (zeros === 0){
      setGameOver(true);
    }
    
    for (let index = 0; index < WINNERS.length; index++) {
      const combo = WINNERS[index];
      
      if (mapa[combo[0]] === mapa[combo[1]] && mapa[combo[1]]===mapa[combo[2]] && mapa[combo[0]]!= 0){
        let aux = mapa[combo[0]];
        setGanador(aux);
      }
    } 
  }, [mapa]);

  return (
    <DivCentrado>
      <Tablero mensaje={mensaje} estado={mapa} clickCelda={(ganador) ? null : clickCelda}></Tablero>
    </DivCentrado>
  );
};
