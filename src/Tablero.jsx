import React, { useState, useEffect } from "react"; // hook
import styled from 'styled-components';

import Panel from './Panel';
import Celda from './Celda';

const Tablero = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 600px;
    border: 3px solid grey;
`;

const Row = styled.div`
    display: flex;
    width: 600px;
    flex-wrap: wrap;
`;


export default (props) => {


    return(
        <Tablero>
            
            <Panel mensaje={props.mensaje}></Panel>
            
            <Row>
                <Celda estado={props.estado[0]} clickCelda={() => props.clickCelda(0)}></Celda>
                <Celda estado={props.estado[1]} clickCelda={() => props.clickCelda(1)}></Celda>
                <Celda estado={props.estado[2]} clickCelda={() => props.clickCelda(2)}></Celda>
            </Row>
            <Row>
                <Celda estado={props.estado[3]} clickCelda={() => props.clickCelda(3)}></Celda>
                <Celda estado={props.estado[4]} clickCelda={() => props.clickCelda(4)}></Celda>
                <Celda estado={props.estado[5]} clickCelda={() => props.clickCelda(5)}></Celda>
            </Row>
            <Row>
                <Celda estado={props.estado[6]} clickCelda={() => props.clickCelda(6)}></Celda>
                <Celda estado={props.estado[7]} clickCelda={() => props.clickCelda(7)}></Celda>
                <Celda estado={props.estado[8]} clickCelda={() => props.clickCelda(8)}></Celda>
            </Row>
        </Tablero>
    );
}