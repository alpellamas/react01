import React, { useState } from "react"; // hook
import './estilos.css';
import styled from 'styled-components'
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

// STYLEDS

const DivCentrado = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

const Titulo1 = styled.h1`
    text-align: center;
    color: blue;
    font-weight: bold;
`;

const Bola = styled.div`
    width: 50px;
    height: 50px;
    border-radius: 50px;
    background-color: ${(props) => props.color || "green"};
    display: inline-block;
    margin: 5px;
`;

const Posicion = styled.div`
        width: 60px;
        padding: 10px;
        font-size: 30px;
        font-weight: bold;
        color: blue;
        border: 1px solid grey;
        text-align: center;
    `;

// COMPONENTES FUNCIONALES

const Titulo = ({texto}) => (
    <DivCentrado>
        <Titulo1>{texto}</Titulo1>
    </DivCentrado>
);

const Mosca = (props) => (
    <div className="mosca">
        <i className="fas fa-dove" style={{color: props.color}} ></i>
    </div>
);

const Capital = (props) => (
    <div className="capital">
        <span className="upper" style={{fontSize : "200px"}}>{props.nom[0]}</span>
        <span className="capital-name">{props.nom}</span>
    </div>
);

const Gato = (props) => {
    const enlace = "http://placekitten.com/"+props.ancho+"/"+props.alto;

    return (
        <div className="divGato">
            <div className="gato">
                <img src={enlace} />
                <span className="gato-nombre">{props.nombre}</span>
            </div>
        </div>
        
    );
}

const BolaBingo = ({numero}) => {
    return (

        <div className="divBola">
            <div className="bola">
                <span className="bola-texto">{numero}</span>
            </div>
        </div>
    );
}

const Semaforo = (props) => {

    return (
        <div className="semaforo">
            {props.children}
        </div>
    );
}

const Circulo = () => {
    let [contador, setContador] = useState(0);
    let colorcillo;

    let clicar = () => {
        
        if(contador === 2){
            setContador(contador = 0);
        }else{
            setContador(contador+1);
        }
    };

    if(contador === 0){
        colorcillo = "red";
    }
    if(contador === 1){
        colorcillo = "yellow";
    }
    if(contador === 2){
        colorcillo = "green";
    }

    //console.log(contador);
    //console.log(colorcillo);

    return (
        <div onClick={clicar} className="circulo" style={{backgroundColor: colorcillo}}></div>
    );
}

const Contador = () => {

    let [contador, setContador] = useState(0); 

    const clicar = (num) => {
        if(num === 0 && contador >0){
            setContador(contador-1);
        }

        if(num === 1 && contador <10){
            setContador(contador+1);
        }
    };


    return (
        <div className="divContador">
            <button onClick={()=>clicar(0)} className="botonContador"> - </button>
            <input className="inputContador" type="text" value={contador} disabled="disabled" />
            <button onClick={()=>clicar(1)} className="botonContador"> + </button>
        </div>
    );
}

const Flipicon = (props) => {
    const Flipicon = styled.i`
        font-size: 80px;
        color: green;
    `;


    let [iconSelected, setIcon] = useState(props.icon1); 

    const clicar = () => {
        if(iconSelected === props.icon1 ){
            setIcon(props.icon2);
        }else{
            setIcon(props.icon1);
        }


    };

    return (
        <DivCentrado>
           <Flipicon onClick={clicar} className={iconSelected} /> 
        </DivCentrado>
    );
}

const Lista = (props) => {

    let [contador, setContador] = useState(0); 

    const clicar = (accion) => { 
        if(accion == 0 && contador >=0){
            setContador(contador-1);
        }
        if(accion == 1 && contador <15){
            setContador(contador+1);
        }
    };

    const mostrar = (numero) => {
        alert(numero);
    }

    return (
        <DivCentrado>
            <Pagination aria-label="Page navigation example">
                <PaginationItem>
                    <PaginationLink previous href="#" onClick={() => clicar(0)} />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#" onClick={() => mostrar(parseInt(contador)+1)}>
                        {parseInt(contador)+1}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#" onClick={() => mostrar(parseInt(contador)+2)}>
                        {parseInt(contador)+2}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#" onClick={() => mostrar(parseInt(contador)+3)}>
                        {parseInt(contador)+3}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#" onClick={() => mostrar(parseInt(contador)+4)}>
                        {parseInt(contador)+4}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink href="#" onClick={() => mostrar(parseInt(contador)+5)}>
                        {parseInt(contador)+5}
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink next href="#" onClick={() => clicar(1)}/>
                </PaginationItem>
                
            </Pagination>
        </DivCentrado>
    );
}

// STYLED GATO


const Marco = styled.div`
    display: flex;
    width: 210px;
    flex-wrap: wrap;
    justify-content: center;
    border: 2px solid purple;
    padding: 5px;
    text-align: center;
`;

const Foto = styled.img`

`;

const PieFoto = styled.span`
    margin-top: 5px;
`;

const StyledGato = (props) => {

    const enlace = "http://placekitten.com/"+props.ancho+"/"+props.alto;
    
    return (
        <DivCentrado>
            <Marco>
                <Foto ancho={props.ancho} alto={props.alto} src={enlace}  />
                <PieFoto>{props.nombre}</PieFoto>
            </Marco>
        </DivCentrado>
    );
}



export {DivCentrado};